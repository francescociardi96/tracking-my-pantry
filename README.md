# Tracking My Pantry 
The project was carried out entirely through the Android Studio platform and the use of the Java language.
"Tracking My Pantry" is a mobile application designed for those who want to track the shopping they buy using their barcode and create a collaborative barcode database that can be used by the community.
